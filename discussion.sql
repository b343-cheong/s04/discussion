-- [Section] Advance Selects

-- Exclude records
SELECT * FROM songs WHERE song_name != "Kudiman";

-- Exclude OPM songs and we want to show the song_name and length column only. (Not case sensitive)
SELECT song_name, length FROM songs WHERE genre != "opm";

SELECT song_name, length FROM songs WHERE genre != "OPM";

--Greater than or equal and Less or Equal Operator
SELECT * FROM songs WHERE length > 300;

SELECT * FROM songs WHERE length < 300;

-- Songs that are from longer than 3 minutes but shorter than 4 minutes
SELECT * FROM songs WHERE length > 300 AND length < 400;

SELECT * FROM songs WHERE length >= 300 AND length <= 416;

-- OR Operator
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 3

-- IN Operator
SELECT * FROM songs WHERE id IN (1,3,5);

SELECT * FROM songs WHERE genre IN ("Pop", "Pop rock");

-- Find Partial matches:
SELECT * FROM songs WHERE song_name LIKE "%a"; -- Finds song end with a

SELECT * FROM songs WHERE song_name LIKE "a%"; -- Finds song beginnning with a

SELECT * FROM songs WHERE song_name LIKE "%y"; -- Finds song end with y

SELECT * FROM songs WHERE song_name LIKE "%Story"; -- Finds song end with story

SELECT * FROM songs WHERE song_name LIKE "%r%"; -- Finds song with letter r anywhere

SELECT * FROM songs WHERE song_name LIKE "%t%";  -- Finds song with letter t anywhere

SELECT * FROM songs WHERE song_name LIKE "%s%o%";  -- Finds song with s and o anywhere

SELECT * FROM songs WHERE song_name LIKE "l%r%"; -- Finds song beginning with l and anywhere for letter r

SELECT * FROM songs WHERE song_name LIKE "l%r%" AND genre = "Pop"; -- Finds song beginning with l and anywhere for letter r, genre should be Pop

SELECT * FROM songs WHERE song_name LIKE "%s%o%" OR song_name LIKE "%o%s%" -- Finds song name either with s and o anywhere or song_name o and s anywhere in order

-- LIKE (INT) (Pattern):
SELECT * FROM albums WHERE album_title LIKE "Tr_p"; -- Finds album title that is exactly Tr()p. () presents anything.

SELECT * FROM albums WHERE album_title LIKE "Tr%";

SELECT * FROM songs WHERE id LIKE "1_"; --Finds songs that has an id of 1(). () presents anything.

SELECT * FROM songs WHERE id LIKE "_"; --Finds songs that has an id of (). () presents anything.

-- Sorting Records:
SELECT * FROM songs ORDER BY song_name ASC;

SELECT * FROM songs ORDER BY song_name DESC;

SELECT * FROM songs WHERE song_name LIKE "%s%o%" ORDER BY song_name ASC;

-- Get distinct records
SELECT DISTINCT genre FROM songs;

-- SECTION: Table Joins
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT artists.name, albums.album_title, albums.date_released FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name 
FROM artists JOIN albums ON artists.id = albums.artist_id 
JOIN songs ON albums.id = songs.album_id WHERE song_name LIKE "l%"
ORDER BY song_name ASC;